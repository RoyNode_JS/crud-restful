'use strict';

module.exports = function(app) {
  var genre = require('../controllers/genreController');
  var book = require('../controllers/bookController');

//Get Genres & Add Genre

app.route('/api/genres')
	.get(genre.getGenres)
	.post(genre.addGenre);

//Get Genre, Update Genre, & Delete Genre
app.route('/api/genre/:id')
	.get(genre.getGenreById)
	.put(genre.updateGenre)
	.delete(genre.removeGenre);


//Get Books & Add Book
app.route('/api/books')
	.get(book.getBooks)
	.post(book.addBook);

//Get Book, Update Book, & Delete Book
app.route('/api/book/:id')
	.get(book.getBookById)
	.put(book.updateBook)
	.delete(book.removeBook);

};
