var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var Genre = require('./models/genre');
var Book = require('./models/book');

//connect to Mongoose
mongoose.connect('mongodb://localhost/bookstore', { useNewUrlParser: true });

app.use(bodyParser.json());

var routes = require('./route/bookstoreRoute'); //importing route
routes(app); //register the route

app.listen(3000);
console.log('Running on port 3000...');
