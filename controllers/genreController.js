'use strict';

var mongoose = require('mongoose'),
  Genre = mongoose.model('Genre');

exports.getGenres = function(req, res){
	Genre.find({}, function(err, genre){
		if(err){
			throw err;
		}
		res.json(genre);
	});
};

exports.getGenreById = function(req, res){
	Genre.findById(req.params._id, function(err, genre){
		if(err){
			throw err;
		}
		res.json(genre);
	});
};

exports.addGenre = function(req, res){
	let new_genre = new Genre(req.body); 
	new_genre.save(function(err, genre){
		if(err){
			throw err;
		}
		res.json(genre);
	});
};

exports.updateGenre= function(req, res){
	Genre.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function(err, genre) {
		if(err)
			throw err;
		
		res.json(genre);
	});
};

exports.removeGenre = function(req, res){
	Genre.deleteOne({_id: req.params.id}, function(err, genre){
		if(err){
			throw err;
		}
		res.json(genre);
	});
};
