'use strict';

var mongoose = require('mongoose'),
  Book = mongoose.model('Book');

exports.getBooks = function(req, res){
	Book.find({}, function(err, book){
		if(err){
			throw err;
		}
		res.json(book);
	});
};

exports.getBookById = function(req, res){
	Book.findById(req.params._id, function(err, book){
		if(err){
			throw err;
		}
		res.json(book);
	});
};

exports.addBook = function(req, res){
	var new_book = new Book(req.body); 
	new_book.save(function(err, book){
		if(err){
			throw err;
		}
		res.json(book);
	});
};

exports.updateBook = function(req, res){
	Book.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function(err, book) {
		if(err){
			throw err;
		}
		
		res.json(book);
	});
};

exports.removeBook = function(req, res){
	Book.remove({_id: req.params.id}, function(err, book){
		if(err){
			throw err;
		}
		res.json(book);
	});
};


