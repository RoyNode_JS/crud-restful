var mongoose = require('mongoose');

//Genre Schema
let bookSchema = mongoose.Schema({
	title:{
		type: String, 
		required: true
	},
	genre:{
		type: String, 
		required: true
	},
	description:{
		type: String, 
		required: true
	},
	author:{
		type: String, 
		required: true
	},
	publisher:{
		type: String, 
	},
	pages:{
		type: String, 
	},
	image_url:{
		type: String, 
	},
	buy_url:{
		type: String, 
	}},
	{ versionKey: false });

module.exports = mongoose.model('Book', bookSchema);
