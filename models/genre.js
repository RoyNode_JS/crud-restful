var mongoose = require('mongoose');

let genreSchema = mongoose.Schema({
	name:{
		type: String, 
		required: true
	}},
	{ versionKey: false });
 //membuat genre bisa diakses dimananapun
module.exports = mongoose.model('Genre', genreSchema);
